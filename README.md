# hsp-aufgabe-berechnung
Führen Sie diverse Berechnungen aus und geben Sie diese als Zeichenkette aus für Hochsprachenprogrammierung / Mecke an der Jade Hochschule

Aufgabe: Implementieren Sie diverse Berechnungen und geben Sie diese innerhalb der Klasse "Program" Program.cs aus. 
* Quadriert eine Zahl in der Methode quadradriereZahl
* Liefert einen Kontext für eine verdoppelte Zahl in der Methode verdoppelterStringMitKontext
* Verdoppelt eine Zahl und gibt einen String mit Nachkommastellen mit Einheit zurück in der Methode verdoppelterStringMitZweiNachkommastellen
* Verdoppelt eine Zahl und gibt einen String mit frei definierbarer Anzahl an Nachkommastellen mit Einheit zurück in der Methode verdoppelterStringMitNachkommastellenInMeter

## Getting Started
* Clonen Sie die Repository auf Ihren Rechner
* Öffnen Sie die Arbeitsmappe (Solution *.sln Datei) in Visual Studio
* Führen Sie die Implementierung in der Methode durch

Wenn Sie fertig sind, dann
* Im Projektmappen Explorer (engl. Solution Explorer) klicken Sie rechts das -Unittesting Projekt und wählen Sie "Tests ausführen"
* Starten Sie die Tests innerhalb des Test-Explorers
