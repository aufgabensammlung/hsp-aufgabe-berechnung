﻿using System;

namespace Berechnung
{
    public class Program
    {

        /// <summary>
        /// Quadriert eine Zahl 
        /// </summary>
        /// <param name="zahl"></param>
        /// <returns>quadrierteZahl</returns>
        public static double quadriereZahl(double zahl)
        {
            // Beispiel: 
            // zahl = (double) 3 zu quadrierenden Zahl
            // Ergebnis (return): (double) 9 

            return 0; // Die 0 ist ein Platzhalter für einen Rückgabe.
                      // Das funktioniert, ist also keine Programmfehler,
                      // aber das Ergebnis ist wahrscheinlich falsch => das müssen Sie ändern 
        }

        /// <summary>
        /// Liefert einen Kontext für die verdoppelte Zahl
        /// </summary>
        /// <param name="zahl"></param>
        /// <returns>verdoppelte Zahl mit Kontext</returns>
        public static string verdoppelterStringMitKontext(double zahl)
        {
            // Beispiel: 
            // zahl = 1.2 - zu verdoppelnde Zahl
            // Ergebnis: "Ergebnis: 2.4"
            // Achtung: Komma oder Punkt ist abh. von Ihrem Rechner. ändern Sie ggf. den Test.

            return "";
        }

        /// <summary>
        /// Verdoppelt eine Zahl und gibt einen String mit zwei Nachkommastellen zurück
        /// </summary>
        /// <param name="zahl">Zahl, die verdoppelt wird</param>
        /// <returns>verdoppelte Zahl als String mit zwei Nachkommastellen</returns>
        public static string verdoppelterStringMitZweiNachkommastellen(double zahl)
        {
            // Beispiel: 
            // zahl = 1.3 - VerdoppelteZahl
            // Ergebnis: "2,60"
            // Tipp: String.Format()...
            return "";
        }

        /// <summary>
        /// Verdoppelt eine Zahl und gibt einen String mit Nachkommastellen mit Einheit zurück
        /// </summary>
        /// <param name="zahl">Zahl, die verdoppelt wird</param>
        /// <param name="nks">Anzahl der Nachkommastellen</param>
        /// <returns>verdoppelte Zahl als String mit Nachkommastellen und Einheit </returns>
        public static String verdoppelterStringMitNachkommastellenInMeter(double zahl, int nks)
        {
            // Beispiel: 
            // nks = 3 - drei Nachkommastellen im String
            // zahl = 1.3 - VerdoppelteZahl
            // Ergebnis: "2,600 m"

            // Hinweis: Es gibt verschiedene Lösungsmöglichkeiten für diese Aufgabe.
            //    eine davon funktioniert mit einer Schleife, welche vielleicht erst später
            //    besprochen wird.

            return "";
        }

        static void Main(string[] args)
        {
            double erg1 = quadriereZahl(1.1);
            Console.WriteLine("Ergebnis als Double: " + erg1);

            String erg2 = Berechnung.Program.verdoppelterStringMitKontext(1.2);
            Console.WriteLine(erg2);

            String erg3 = Berechnung.Program.verdoppelterStringMitZweiNachkommastellen(1.3);
            Console.WriteLine("Ergebnis mit NKs: " + erg3);

            String erg4 = Berechnung.Program.verdoppelterStringMitNachkommastellenInMeter(1.4,2);
            Console.WriteLine("Ergebnis mit NKs: " + erg4);

            Console.WriteLine("Berechnungen abgeschlossen!");
            Console.ReadLine();
        }

    }
}
