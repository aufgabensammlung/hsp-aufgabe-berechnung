using Microsoft.VisualStudio.TestTools.UnitTesting;
using Berechnung;

namespace Berechnung_UnitTests
{
    [TestClass]
    public class UnitTest1
    {
        #region quadrieren
        [TestMethod]
        public void quadrieren_T1()
        {        
            // Act 
            var result = Berechnung.Program.quadriereZahl(1.1);

            // Assert
            Assert.AreEqual(result, 1.1*1.1);
        }

        [TestMethod]
        public void quadrieren_T2()
        {
            // Act 
            var result = Berechnung.Program.quadriereZahl(3.0);

            // Assert
            Assert.AreEqual(result, 9);
        }

        [TestMethod]
        public void quadrieren_T3()
        {
            // Act 
            var result = Berechnung.Program.quadriereZahl(-2.0);

            // Assert
            Assert.AreEqual(result, 4);
        }
        #endregion

        #region mitKontext
        [TestMethod]
        public void mitKontext_T1()
        {
            // Act 
            var result = Berechnung.Program.verdoppelterStringMitKontext(1.2);

            // Assert
            Assert.AreEqual(result, "Ergebnis: 2,4");
        }

        [TestMethod]
        public void mitKontext_T2()
        {
            // Act 
            var result = Berechnung.Program.verdoppelterStringMitKontext(3.0);

            // Assert
            Assert.AreEqual(result, "Ergebnis: 6");
        }

        [TestMethod]
        public void mitKontext_T3()
        {
            // Act 
            var result = Berechnung.Program.verdoppelterStringMitKontext(-2.0);

            // Assert
            Assert.AreEqual(result, "Ergebnis: -4");
        }
        #endregion

        #region mitZweiNachkommastellen
        [TestMethod]
        public void mitZweiNachkommastellen_T1()
        {
            // Act 
            var result = Berechnung.Program.verdoppelterStringMitZweiNachkommastellen(1.1);

            // Assert
            Assert.AreEqual(result, "2,20");
        }

        [TestMethod]
        public void mitZweiNachkommastellen_T2()
        {
            // Act 
            var result = Berechnung.Program.verdoppelterStringMitZweiNachkommastellen(3.0);

            // Assert
            Assert.AreEqual(result, "6,00");
        }

        [TestMethod]
        public void mitZweiNachkommastellen_T3()
        {
            // Act 
            var result = Berechnung.Program.verdoppelterStringMitZweiNachkommastellen(-2.0);

            // Assert
            Assert.AreEqual(result, "-4,00");
        }
        #endregion

        #region mitNachkommastellenInMeter
        [TestMethod]
        public void mitNachkommastellenInMeter_T1()
        {

            // Act 
            var result = Berechnung.Program.verdoppelterStringMitNachkommastellenInMeter(1.1, 3);

            // Assert
            Assert.AreEqual(result, "2,200 m");
        }

        [TestMethod]
        public void mitNachkommastellenInMeter_T2()
        {

            // Act 
            var result = Berechnung.Program.verdoppelterStringMitNachkommastellenInMeter(3.0, 0);

            // Assert
            Assert.AreEqual(result, "6 m");
        }

        [TestMethod]
        public void mitNachkommastellenInMeter_T3()
        {

            // Act 
            var result = Berechnung.Program.verdoppelterStringMitNachkommastellenInMeter(-2.001, 4);

            // Assert
            Assert.AreEqual(result, "-4,0020 m");
        }
        #endregion


    }
}
